FROM hypriot/rpi-alpine-scratch

RUN apk update && \
    apk upgrade && \
    apk add bash && \
    apk add --no-cache bash git openssh && \
    rm -rf /var/cache/apk/*

VOLUME /git

ENV DEST=. \
    GIT_COMMIT=master \
    GIT_HOST=github.com \
    GIT_REPO=uri \
    INTERVAL=0

COPY entrypoint.sh /root/
RUN chmod a+x /root/entrypoint.sh
ENTRYPOINT /root/entrypoint.sh
